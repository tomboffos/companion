package middleware

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/companion/pkg/user/services"
)

func UserMiddleware() gin.HandlerFunc {
	return func(context *gin.Context) {
		tokenString := context.GetHeader("Authorization")

		if tokenString == "" {
			context.JSON(401, gin.H{"error": "request does not contain an access token"})
			context.Abort()
			return
		}

		_, err := services.ParseToken(tokenString)

		if err != nil {
			context.JSON(401, gin.H{"error": err.Error()})
			context.Abort()
			return
		}
		context.Next()
	}
}
