package places

import "github.com/gin-gonic/gin"

func RegisterRoutes(r *gin.Engine) {
	routes := r.Group("places")
	{
		routes.POST("/", CreatePlace)
		routes.GET("/:value", GetPlaces)
	}
}
