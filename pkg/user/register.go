package user

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/companion/pkg/common/models"
	"gitlab.com/tomboffos/companion/pkg/user/services"
	"net/http"
)

type RegisterBody struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
	Email    string `json:"email"`
}

func (h handler) Register(c *gin.Context) {
	body := RegisterBody{}

	if err := c.BindJSON(&body); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	var user models.User

	user.Username = body.Username
	user.Password = body.Password
	user.Email = body.Email

	if result := h.DB.Create(&user); result.Error != nil {
		c.AbortWithError(http.StatusInternalServerError, result.Error)
		return
	}

	tokenString, err := services.GenerateJWT(user)

	if err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
	}

	c.JSON(http.StatusCreated, gin.H{
		"user":  user,
		"token": tokenString,
	})
}
