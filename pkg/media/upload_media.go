package media

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/companion/pkg/common/models"
	"gitlab.com/tomboffos/companion/pkg/user/services"
	"net/http"
	"path/filepath"
	"strconv"
	"time"
)

func (h handler) UploadMedia(c *gin.Context) {

	file, err := c.FormFile("file")

	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	user, err := services.ParseToken(c.GetHeader("Authorization"))

	filePath := filepath.Join("uploads/"+user.Username, strconv.FormatInt(int64(time.Now().Unix()), 10)+"-"+file.Filename)

	if err := c.SaveUploadedFile(file, filePath); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to save file"})
		return
	}

	var media models.Media

	boolVal, err := strconv.ParseBool(c.PostForm("is_main"))

	if boolVal {
		var medias models.Media

		err := h.DB.Model(&medias).Where("user_id = ?", user.ID).Update("is_main", false).Error

		if err != nil {
			c.AbortWithError(http.StatusInternalServerError, err)
			return
		}
	}

	media.IsMain = boolVal
	media.File = filePath
	if user.ID != 0 {
		media.UserID = &user.ID
	}

	if result := h.DB.Create(&media); result.Error != nil {
		c.AbortWithError(http.StatusInternalServerError, result.Error)
		return
	}

	c.JSON(http.StatusCreated, gin.H{
		"media": media,
	})
	return
}
