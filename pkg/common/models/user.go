package models

import (
	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	Username   string  `json:"username" gorm:"size:255; not null; unique"`
	Password   string  `json:"password" gorm:"size:255; not null;" `
	Email      string  `json:"email" gorm:"size:255;" `
	UserMedias []Media `json:"user_medias"`
	About      string  `json:"about"`
}

func VerifyPassword(password, hashedPassword string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func (u *User) BeforeSave(tx *gorm.DB) error {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(u.Password), bcrypt.DefaultCost)

	if err != nil {
		return err
	}

	u.Password = string(hashedPassword)

	return nil
}
