package media

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/companion/pkg/common/models"
	"net/http"
	"os"
)

func (h handler) GetMedia(c *gin.Context) {
	id := c.Param("id")

	var media models.Media

	if err := h.DB.First(&media, id).Error; err != nil {
		c.AbortWithError(http.StatusNotFound, err)
		return
	}

	fileData, err := os.Open(media.File)

	if err != nil {
		c.AbortWithError(http.StatusNotFound, err)
		return
	}

	defer fileData.Close()

	fileHeader := make([]byte, 512)

	_, err = fileData.Read(fileHeader)

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to read file"})
		return
	}

	fileContentType := http.DetectContentType(fileHeader)

	fileInfo, err := fileData.Stat()

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get file info"})
		return
	}
	c.Header("Content-Description", "File Transfer")
	c.Header("Content-Transfer-Encoding", "binary")
	c.Header("Content-Disposition", fmt.Sprintf("attachment; filename=%s", media.File))
	c.Header("Content-Type", fileContentType)
	c.Header("Content-Length", fmt.Sprintf("%d", fileInfo.Size()))
	c.File(media.File)
}
