package places

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/companion/pkg/common/config"
	"gitlab.com/tomboffos/companion/pkg/user/services"
	"io/ioutil"
	"net/http"
)

type CreatePlaceBody struct {
	Title   string `json:"title"   binding:"required"`
	Address string `json:"address" binding:"required"`
}

type PlaceResponse struct {
	Title   string `json:"title"`
	Address string `json:"address"`
	UserId  uint   `json:"user_id"`
	ID      uint   `json:"ID"`
}

func CreatePlace(c *gin.Context) {
	body := CreatePlaceBody{}

	if err := c.BindJSON(&body); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	client := http.Client{}

	placesApi := config.GetEndpoints().PlacesApi

	user, err := services.ParseToken(c.GetHeader("Authorization"))

	if err != nil {
		c.AbortWithError(http.StatusUnauthorized, err)
		return
	}

	byteBody := []byte(fmt.Sprintf(`{
		"title": "%s",
		"address": "%s",
		"user_id": %d
	}`, body.Title, body.Address, user.ID))

	resp, err := client.Post(placesApi+"/places", "application/json", bytes.NewBuffer(byteBody))

	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	place := &PlaceResponse{}

	responseBody, err := ioutil.ReadAll(resp.Body)

	json.Unmarshal(responseBody, &place)

	c.JSON(http.StatusCreated, gin.H{
		"place": place,
	})
}
