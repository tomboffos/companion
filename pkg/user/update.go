package user

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/companion/pkg/common/models"
	"gitlab.com/tomboffos/companion/pkg/user/services"
	"net/http"
)

type UpdateUserBody struct {
	Username string `json:"username"`
	Password string `json:"password"`
	Email    string `json:"email"`
	About    string `json:"about"`
}

func (h handler) UpdateUser(c *gin.Context) {
	body := UpdateUserBody{}

	if err := c.BindJSON(&body); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	user, _ := services.ParseToken(c.GetHeader("Authorization"))

	var userModel models.User

	if result := h.DB.First(&userModel, user.ID); result.Error != nil {
		c.AbortWithError(http.StatusNotFound, result.Error)
		return
	}

	userModel.Username = body.Username
	userModel.Password = body.Password
	userModel.Email = body.Email
	userModel.About = body.About

	h.DB.Save(&userModel)

	c.JSON(http.StatusOK, &userModel)
	return
}
