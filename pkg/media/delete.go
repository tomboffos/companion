package media

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/companion/pkg/common/models"
	"net/http"
	"os"
)

func (h handler) Delete(c *gin.Context) {
	id := c.Param("id")

	var media models.Media

	if result := h.DB.First(&media, id); result.Error != nil {
		c.AbortWithError(http.StatusNotFound, result.Error)
		return
	}

	err := os.Remove(media.File)

	err = h.DB.Delete(&media).Error

	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete file from database"})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Media deleted successfully",
	})
}
