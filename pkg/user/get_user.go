package user

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/companion/pkg/common/models"
	"gitlab.com/tomboffos/companion/pkg/user/services"
	"net/http"
)

func (h handler) GetUser(c *gin.Context) {
	tokenString := c.GetHeader("Authorization")

	user, err := services.ParseToken(tokenString)

	if err != nil {
		c.AbortWithError(http.StatusNotFound, err)
		return
	}

	var userModel models.User

	queryErr := h.DB.Preload("UserMedias").First(&userModel, user.ID).Error

	if queryErr != nil {
		c.AbortWithError(http.StatusInternalServerError, queryErr)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"user": userModel,
	})
}
