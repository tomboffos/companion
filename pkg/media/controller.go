package media

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/companion/pkg/user/middleware"
	"gorm.io/gorm"
)

type handler struct {
	DB *gorm.DB
}

func RegisterRoutes(r *gin.Engine, db *gorm.DB) {
	h := &handler{
		DB: db,
	}

	routes := r.Group("media")
	{
		routes.POST("/", h.UploadMedia)
		routes.GET("/:id", h.GetMedia)
		routes.DELETE(":id", h.Delete).Use(middleware.UserMiddleware())
	}
}
