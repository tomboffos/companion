package places

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/companion/pkg/common/config"
	"io/ioutil"
	"net/http"
)

type GetPlacesResponse struct {
	Places []PlaceResponse `json:"places"`
}

func GetPlaces(c *gin.Context) {
	value := c.Param("value")

	client := http.Client{}

	placesApi := config.GetEndpoints().PlacesApi

	resp, err := client.Get(placesApi + "/places/" + value)

	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	places := &GetPlacesResponse{}

	responseBody, err := ioutil.ReadAll(resp.Body)

	json.Unmarshal(responseBody, &places)

	c.JSON(http.StatusOK, places)
}
