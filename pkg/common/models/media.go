package models

import "gorm.io/gorm"

type Media struct {
	gorm.Model
	File   string `json:"file"`
	IsMain bool   `json:"is_main"`
	UserID *uint  `json:"user_id"`
}
