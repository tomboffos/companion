package main

import (
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"gitlab.com/tomboffos/companion/pkg/common/db"
	"gitlab.com/tomboffos/companion/pkg/media"
	"gitlab.com/tomboffos/companion/pkg/places"
	"gitlab.com/tomboffos/companion/pkg/user"
	"gorm.io/gorm"
)

func setupRouter(h *gorm.DB) *gin.Engine {
	r := gin.Default()
	user.RegisterRoutes(r, h)
	media.RegisterRoutes(r, h)
	places.RegisterRoutes(r)

	return r
}

func main() {
	viper.SetConfigFile("./pkg/common/envs/.env")
	viper.ReadInConfig()

	port := viper.Get("PORT").(string)
	dbUrl := viper.Get("DB_URL").(string)

	h := db.Init(dbUrl)
	r := setupRouter(h)

	r.Run(port)
}
