package user

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/companion/pkg/common/models"
	"gitlab.com/tomboffos/companion/pkg/user/services"
	"net/http"
)

type LoginBody struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

func (h handler) Login(c *gin.Context) {
	body := LoginBody{}

	if err := c.BindJSON(&body); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	var user models.User

	result := h.DB.Where("username=?", body.Username).Find(&user)

	if result.Error != nil {
		c.AbortWithError(http.StatusNotFound, result.Error)
		return
	}

	if err := models.VerifyPassword(body.Password, user.Password); err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}
	tokenString, err := services.GenerateJWT(user)

	if err != nil {
		c.AbortWithError(http.StatusBadRequest, err)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"token": tokenString,
		"user":  user,
	})
}
