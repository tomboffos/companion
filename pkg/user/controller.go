package user

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/tomboffos/companion/pkg/user/middleware"
	"gorm.io/gorm"
)

type handler struct {
	DB *gorm.DB
}

func RegisterRoutes(r *gin.Engine, db *gorm.DB) {
	h := &handler{
		DB: db,
	}

	routes := r.Group("user")
	{
		routes.POST("/register", h.Register)
		routes.POST("/login", h.Login)

		secured := routes.Group("/").Use(middleware.UserMiddleware())
		{
			secured.GET("/", h.GetUser)
			secured.PUT("/", h.UpdateUser)
		}
	}
}
